FROM pytorch/pytorch
RUN apt-get update && apt-get install -y curl bash
RUN apt-get install -y git
ENV GIT_PYTHON_GIT_EXECUTABLE=/usr/bin/git
RUN echo $PATH

# Установка pixi
RUN mkdir /block4project
WORKDIR /root
RUN curl -fsSL https://pixi.sh/install.sh | bash

# Добавляем PATH явно, потому что Docker не использует логин-шелл по умолчанию
ENV PATH="/root/.pixi/bin:${PATH}"

# Проверяем, правильно ли установлен pixi
RUN echo $PATH
RUN ls -la /root/.pixi/bin/
RUN which pixi || echo "pixi not found in PATH"

RUN mkdir -p /block4project/.pixi

WORKDIR /block4project
COPY pyproject.toml /block4project
#RUN pixi init /block4project
WORKDIR /block4project
CMD ["pixi", "run", "-e", "dev", "jupyter", "lab", "--ip=0.0.0.0", "--port=8080","--no-browser", "--allow-root", "--ServerApp.token=12345678"]
