import random
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt
import plotly.graph_objs as go
import folium


#from lightgbm import LGBMRegressor


def prepare_random(seed):
    """
    Initializes random state for random and np.random

    Inputs:
        seed - random seed value
    """
    RANDOM_STATE = seed
    random.seed(RANDOM_STATE)
    np.random.seed(RANDOM_STATE)

def print_html(message):
    """
    Outputs a message in HTML format

    Inputs:
        message (string) - HTML text to print
    """
    from IPython.display import display, HTML
    out_msg = HTML(message)
    display(out_msg)

def draw_missing_values_pieplot(data):
    """
    Draws a pieplot for missing values

    Inputs:
        data - incoming data (pandas DataFrame)

    Result:
        Draws pie plot, showing missing values per feature

    Returns:
        Doesn't return any values.

    """
    missing_data = data.isnull().sum()
    missing_data = missing_data[missing_data > 0]  # Оставляем только столбцы с пропусками

    labels = missing_data.index
    values = missing_data.values

    fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
    fig.update_layout(title_text="Процентное соотношение пропущенных значений по столбцам")
    fig.show()

def draw_corr_matrix(data):
    """
    Draws confusion matrix for a given data object

    Inputs:
        data - Pandas DataFrame
    """

    # Корреляционная матрица
    corr = data.corr()

    plt.figure(figsize=(20, 15))

    sns.heatmap(corr, annot=True, cmap='coolwarm', fmt=".2f", linewidths=.5)
    plt.show()

def draw_pointmap(data):
    """
    Draws a geographic map for a given data

    Inputs:
        data - Pandas DataFrame. Must contain latitude and longitude columns
    """

    # Создаем карту
    # Центрируем на среднем значении координат
    pointmap = folium.Map(location=[data['latitude'].mean(), data['longitude'].mean()], zoom_start=11)

    sample_data = data.sample(frac=0.01)  # Подвыборка 1% от данных. Можно подправить

    for idx, row in sample_data.iterrows():
        folium.CircleMarker(
            location=[row['latitude'], row['longitude']],
            radius=1.5,  # Радиус можно подправить
            color='green',
            fill=True,
            fill_color='green'
        ).add_to(pointmap)

    return pointmap